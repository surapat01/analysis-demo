'''
    Example demo for physics analysis

    1. Generate simple MC data - ROOT database 
    2. Perform mass fit - RooFit 

    by S. Ek-In [ surapat.eki@gmail.com ]
'''

def main(): 
    # Step 1 Generate Data 
    from Analysis import Database
    
    database = Database.Generator("Demo database", file_name = "demo", treeName = "sample_demo")
    database.Generate(save_format = "ROOT")
    print("Hi !!")
    
    # Step 2 Make selection 
    data_sample = database.GetData()
    print(data_sample)
    
    # Step 3 Perform fitting 
    from Analysis import Fitter 
    
    config_pdfs = {
                    "Signal_model"     : "Gaussian", 
                    "Background_model" : "Chebychev", 
                    }
    
    fitter = Fitter.Fitter(config = config_pdfs)
    fitter.SetData(data = data_sample, var_fit = "Mass")
    fitter.fit()
    fitter.printParam()
    fitter.SavePlot("MassFit.pdf")
    
    # Step 4 Close file
    database.CloseFile()

if __name__ == "__main__":
    main()

