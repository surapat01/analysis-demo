from setuptools import setup

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='Package',
   version='0.1.0',
   description='A useful module',
   license="MIT",
   long_description=long_description,
   author='Surapat Ek-In',
   packages=['Analysis'],  #same as name
   install_requires=[
       'numpy >= 1.9', 
       ], #external packages as dependencies
)
