from ROOT import RooFit as RF
import ROOT
from Analysis import PlotUtils as PU

 
class Fitter(object):
    '''
        Main object for fitter 
    '''

    def __init__(self, config: dict):
        self.SetModel(config)

    def SetModel(self, config: dict): 
        self.sig_model = config["Signal_model"]
        self.bkg_model = config["Background_model"]

        self.model = ModelPDF(self.sig_model, self.bkg_model)
        self.model.DefineModel()

    def SetData(self, data: ROOT.TTree, var_fit:str): 
        self.dataset = ROOT.RooDataSet("data","data", data, ROOT.RooArgSet(self.model.obs))

    def fit(self): 
        self.fit_result = self.model.ModelPDF.fitTo(
                                                    self.dataset, ROOT.RooFit.Save(), 
                                                     )
    def printParam(self): 
        self.fit_result.Print()


    def SavePlot(self, SaveName):
        PU.Plot_Fit(
                     "Signal", self.dataset, "M(K^{0}_{S}#pi^{-}#pi^{+})", 70, 
                     self.model.obs, self.model.ModelPDF, "SigModel", "BkgModel", "./plots/"+SaveName 
                     ) 

class ModelPDF(object):
    '''
        Class for constructing PDFs
    '''

    def __init__(self, sig_model, bkg_model):
        self.sigPDF_name = sig_model
        self.bkgPDF_name = bkg_model
        

    def DefineModel(self):
        self.DefObservables()
        self.DefSignalModel()
        self.DefBkgModel()
        self.DefPDF()

    def DefObservables(self):
        self.obs = ROOT.RooRealVar("Mass", "Mass", 1795, 1935)

    def DefBkgModel(self):
        if self.bkgPDF_name == "Chebychev":
            self.coef0 = ROOT.RooRealVar("c0","coefficient #0", -0.015, -1.0, 0.5)
            self.BkgParams = [self.coef0]
            self.BkgModel = ROOT.RooChebychev("BkgModel", "BkgModel", self.obs, ROOT.RooArgList(self.coef0))
        else:
            print("Please specify self.bkgPDF_name")
            print(("Now self.bkgPDF_name = {}".format(self.bkgPDF_name)))
            raise ValueError


    def DefSignalModel(self):

        if self.sigPDF_name == "Gaussian":
            # Main used
            self.Gauss_mean = ROOT.RooRealVar("Gauss_mean","Gauss_mean",  1865.4,1861.0,1872)
            self.Gauss_width = ROOT.RooRealVar("Gauss_width","Gauss_width", 7, 6, 15   )


            # Gaussian part
            self.SigModel =  ROOT.RooGaussian("SigModel", "SigModel", self.obs,
                                                self.Gauss_mean, 
                                                self.Gauss_width)

            self.SigParams = [self.Gauss_mean, self.Gauss_width]

        else:
            print("Please specify self.sigPDF_name")
            print(("Now self.sigPDF_name = {}".format(self.sigPDF_name)))
            raise ValueError

    def DefPDF(self):
        self.Nsig = ROOT.RooRealVar("Nsig", "Nsig", 0.0, 10000)
        self.Nbkg = ROOT.RooRealVar("Nbkg", "Nbkg", 0.0, 10000)

        self.ModelPDF = ROOT.RooAddPdf("ModelPDF", "ModelPDF",
                                  ROOT.RooArgList(self.SigModel, self.BkgModel),
                                  ROOT.RooArgList(self.Nsig, self.Nbkg))

