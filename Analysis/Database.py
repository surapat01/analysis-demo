import ROOT
import numpy as np
from tqdm import tqdm
from array import array

data_path = "./data"


class Database(object):

    '''
        Main database
    '''


    def __init__(self, name:str, file_name:str, treeName:str) -> None:
        self.name = name
        self.fileOutName = f"{data_path}/{file_name}"
        self.treeName = treeName


    def GetData(self) -> ROOT.RooDataSet:

        
        self.file = ROOT.TFile(self.fileOutName)
        self.treeMain = self.file.Get(self.treeName)
        print(self.treeMain)
        return self.treeMain

    def CloseFile(self): 
        self.file.Close()


class Generator(Database):
    '''
        Generate MC sample in ROOT format 
    '''

    def DefineConf(self):
        # Configuration 
        self.Mean  = 1864.84
        self.Width = 12.
        self.exp   = 1.e3
        self.DT_exp = 1.
        self.fraction = 0.5

        # limit 
        self.Mass_min = 1795
        self.Mass_max = 1935
        self.DT_min   = 0
        self.DT_max   = 5


    def SetVariables(self) -> None:

        # Variables
        self.dict_var = {
                    "Mass"           : "array('f', [0.])",
                    "DecayTime_norm" : "array('f', [0.])",
                    }

    def SetFileout(self, filename: str, treeName: str) -> None:

        print(f"Creating {filename}")
        self.fileout = ROOT.TFile(filename, "RECREATE")
        self.tree = ROOT.TTree(treeName, treeName)

        # Set Branch
        self.SetDeclareBranches()
        self.SetBranches()


    def SetDeclareBranches(self) -> None:
        code = "self.{varname} = {value}"
        for varname, value in self.dict_var.items():
            dec_var = code.format(varname = varname, value = value)
            exec(dec_var)

    def SetBranches(self) -> None:
        for varname, value in self.dict_var.items():
            self.tree.Branch(varname, getattr(self, varname), f'{varname}/F')

    def GetRandom(self, var1: float, var2: float = None, dist: str = "uniform") -> float:
        if dist.lower() == "uniform": 
            # var 1 == min ; var 2 == max
            return np.random.uniform(var1, var2)
        elif dist.lower() == "normal":
            # var 1 == mu ; var 2 == sigma
            return np.random.normal(var1, var2)
        elif dist.lower() == "exp":
            # var 1 == beta from dist (x) = 1/beta exp (-x/beta)
            return np.random.exponential(var1)
        else:
            raise TypeError(f"Distribution is not supported: {dist}")


    def ResetVal(self):
        code = "self.{varname}[0] = 0"
        for varname, value in self.dict_var.items():
            line = code.format(varname = varname)
            exec(line)

    def IsInsideLimit(self) -> None:
        check_mass = (self.Mass[0] > self.Mass_min) and (self.Mass[0] < self.Mass_max)
        check_DT   = (self.DecayTime_norm[0] > self.DT_min) and (self.DecayTime_norm[0] < self.DT_max)
        return all([check_mass, check_DT])

    def Generate(self, save_format: str, gen_number_max :int = 1000) -> None:

        # Settings 
        self.fileOutName = "{}.{}".format(self.fileOutName, save_format.lower())

        self.DefineConf()
        self.SetVariables()
        self.SetFileout(self.fileOutName, self.treeName)

        # Generation step 
        n_gen = 0
        print("Generating!")
        pbar = tqdm(total = gen_number_max)
        while (n_gen < gen_number_max):
            # Reset
            self.ResetVal()

            # Is background or signal? 
            if self.GetRandom(0, 1) > self.fraction: 
                while True:
                    # Signal 
                    self.Mass[0]           = self.GetRandom(self.Mean, self.Width, dist = "normal")
                    self.DecayTime_norm[0] = self.GetRandom(self.DT_exp, dist = "exp")
                    # Reject
                    if self.IsInsideLimit():  
                        break
            else: 
                # Background
                self.Mass[0]           = self.GetRandom(self.Mass_min, self.Mass_max, dist = "uniform")
                self.DecayTime_norm[0] = self.GetRandom(self.DT_min, self.DT_max, dist = "uniform")


            # Write to Tree
            self.tree.Fill()

            # Gen success!
            pbar.update(1)
            n_gen += 1
        pbar.close()

        # Write to file
        self.WriteCloseFile()


    def WriteCloseFile(self) -> None:
        self.tree.Write()
        self.fileout.Close()



