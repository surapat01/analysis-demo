import ROOT


def Plot_Fit(name, data, xtitle, NBins, xaxis, FitModel,
             FitModel_Name, FitBkgModel_Name, SaveName, *args):

    c = ROOT.TCanvas("Canvas_"+name,"Canvas_"+name,1024,768)
    ROOT.gPad.SetLogy()

    # Plot Pull - Set Pad 
    pad_fit  = ROOT.TPad("pad_fit" +name, "pad_fit" , 0., 0.20, 1.0, 1.0 )
    pad_pull = ROOT.TPad("pad_pull"+name, "pad_pull", 0., 0.00, 1.0, 0.215)
    pad_fit.Draw()
    pad_pull.Draw()

    # Part 1 Fit
    pad_fit.cd()

    xplot = xaxis.frame(NBins) # may include argument for binning
    data.plotOn(xplot, ROOT.RooFit.Name('Data'))
    FitModel.plotOn(xplot, ROOT.RooFit.Name('Signal+Bkg.'), ROOT.RooFit.Components(FitModel.GetName()),ROOT.     RooFit.LineColor(ROOT.kBlue),ROOT.RooFit.LineStyle(1))
    xplot.SetTitle("")

    xplot.Draw()
    c.Update()

    xplot.SetMaximum(1.2*pad_fit.GetUymax())
    xplot.SetMinimum(0)

    # Part 2 Pull
    pad_pull.cd()

    pad_pull.SetBottomMargin(0.35)
    pull_plot = xaxis.frame(NBins)
    pull_plot.Draw()
    c.Modified()
    c.Update()
    line1 = ROOT.TLine(pad_pull.GetUxmin(),-2, pad_pull.GetUxmax(),-2)
    line1.SetLineColorAlpha(ROOT.kRed, 0.6)
    line1.SetLineStyle(2)
    line2 = ROOT.TLine(pad_pull.GetUxmin(),2, pad_pull.GetUxmax(),2)
    line2.SetLineColorAlpha(ROOT.kRed, 0.6)
    line2.SetLineStyle(2)
    line1.Draw("same")
    line2.Draw("same")

    pull_hist = xplot.pullHist()
    pull_hist.SetFillColor(16)
    pull_hist.SetFillStyle(1001)
    pull_hist.Draw("B")
    pull_hist.Draw("P")

    pull_plot.SetMaximum( 5.9)
    pull_plot.SetMinimum(-5.9)

    pull_plot.SetTitle("")
    pull_plot.GetXaxis().SetTitle(xtitle)
    pull_plot.GetYaxis().SetTitle("Pull")
    pull_plot.GetXaxis().SetLabelSize(0.12)
    pull_plot.GetYaxis().SetLabelSize(0.12)
    pull_plot.GetXaxis().SetTitleSize(0.12)
    pull_plot.GetYaxis().SetTitleSize(0.12)
    pull_plot.GetYaxis().SetTitleOffset(0.20)

    ROOT.gPad.SetTicks()
    c.Update()


    # Get back
    pad_fit.cd()
    xplot.SetXTitle(xtitle)
    xplot.Draw()
    xplot.GetYaxis().SetTitleOffset(1.4)
    #pad_fit.SetLogy()

    leg1 = ROOT.TLegend(0.65,0.75,0.90,0.90)
    FitModel.plotOn(xplot, ROOT.RooFit.Name('Signal')    , ROOT.RooFit.Components(FitModel_Name),ROOT.           RooFit.LineColor(ROOT.kRed),ROOT.RooFit.LineStyle(2))
    FitModel.plotOn(xplot, ROOT.RooFit.Name('Combi. Bkg.'), ROOT.RooFit.Components(FitBkgModel_Name),ROOT.       RooFit.LineColor(416),ROOT.RooFit.LineStyle(2))
    xplot.Draw()

    leg1.AddEntry('Data'          ,"Data"         , "EP")
    leg1.AddEntry('Signal+Bkg.'   ,"Total", "L")
    leg1.AddEntry('Combi. Bkg.'   ,"Combi. bkg."  , "L")
    leg1.AddEntry('Signal'        , name          , "L")


    leg1.Draw('same')
    c.Update()
    c.Modified()
    c.SaveAs(SaveName)


